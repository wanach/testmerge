import { merge } from "../src/merge";

describe('merge', () => {
    it('should merge three sorted collections', () => {
        const collection1 = [5, 4, 3, 2, 1];
        const collection2 = [0, 1, 2, 3, 4];
        const collection3 = [0, 1, 2, 3, 4];
        const result = merge(collection1, collection2, collection3);
        expect(result).toEqual([5, 4, 4, 3, 3, 2, 2, 1, 1, 0, 0, 1, 2, 3, 4]);
    });
});
