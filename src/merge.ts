export function merge(collection1: number[], collection2: number[], collection3: number[]): number[] {
    const result: number[] = [];
    let i = 0, j = 0, k = 0;

    while (i < collection1.length || j < collection2.length || k < collection3.length) {
        const min1 = i < collection1.length ? collection1[i] : Number.MAX_VALUE;
        const min2 = j < collection2.length ? collection2[j] : Number.MAX_VALUE;
        const max3 = k < collection3.length ? collection3[k] : Number.MIN_VALUE;

        if (min1 <= min2 && min1 <= max3) {
            result.push(min1);
            i++;
        } else if (min2 <= min1 && min2 <= max3) {
            result.push(min2);
            j++;
        } else {
            result.push(max3);
            k++;
        }
    }

    return result;
}